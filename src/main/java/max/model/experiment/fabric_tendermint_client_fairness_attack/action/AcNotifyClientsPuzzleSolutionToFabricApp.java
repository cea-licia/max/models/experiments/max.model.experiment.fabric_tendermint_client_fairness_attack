/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.experiment.fabric_tendermint_client_fairness_attack.action;


import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.ledger.simplefabric.env.FabricEnvironment;
import max.model.ledger.simplefabric.role.RFabricApplication;
import max.model.ledger.simplefabric.role.RFabricChannel;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransactionGenerator;
import max.model.ledger.simplefabric.action.comms.AcSubmitEndorsementRequestToEndorsing;
import max.model.ledger.simplefabric.agent.FabricApplication;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;

import java.math.BigDecimal;
import java.util.*;


/**
 * Action executed by the environment to simulate
 * a client broadcasting a new puzzle solution
 * to a Hyperledger Fabric application
 *
 * @author Erwan Mahe
 */
public class AcNotifyClientsPuzzleSolutionToFabricApp extends Action<FabricEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>> {

    private List<String> clientsNames;

    private Optional<DelaySpecification> commonDelaySpecification;

    private HashMap<String,DelaySpecification> addedDelaysPerClient;

    public AcNotifyClientsPuzzleSolutionToFabricApp(String environment,
                                                    FabricEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> owner,
                                                    List<String> clientsNames,
                                                    Optional<DelaySpecification> commonDelaySpecification,
                                                    HashMap<String,DelaySpecification> addedDelaysPerClient) {
        super(environment, RFabricChannel.class, owner);
        this.clientsNames = clientsNames;
        this.commonDelaySpecification = commonDelaySpecification;
        this.addedDelaysPerClient = addedDelaysPerClient;
    }


    @Override
    public void execute() {
        List<String> shuffledClientsForFairness = new ArrayList<>(this.clientsNames);
        for (int i=0; i< 100; i++) {
            // shuffle several times to ensure fairness
            Collections.shuffle(shuffledClientsForFairness);
        }
        for (String clientName : shuffledClientsForFairness) {
            // create transaction
            PuzzleMockupTransaction tx = PuzzleMockupTransactionGenerator.getInstance().createNewTransactionForClient(clientName);
            // ***
            int baseline_delay_for_client = 0;
            if (this.addedDelaysPerClient.containsKey(clientName)) {
                baseline_delay_for_client += this.addedDelaysPerClient.get(clientName).get_concrete_delay();
            }
            // ***
            boolean foundApp = false;
            for (AgentAddress agentAddress : this.getOwner().getAgentsWithRole(this.getEnvironment(), RFabricApplication.class)) {
                FabricApplication<PuzzleMockupTransaction> agent = (FabricApplication<PuzzleMockupTransaction>) agentAddress.getAgent();
                getOwner().getLogger().info(
                        "client " + tx.clientName +
                                " sending solution to puzzle " + tx.puzzleIdentifier +
                                " to application " + agent.getName());
                Action<?> action = new AcSubmitEndorsementRequestToEndorsing<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState, FabricApplication<PuzzleMockupTransaction>>(
                        this.getEnvironment(),
                        agent,
                        tx);
                int delay_sum = baseline_delay_for_client;
                if (this.commonDelaySpecification.isPresent()) {
                    delay_sum = delay_sum + this.commonDelaySpecification.get().get_concrete_delay();
                }
                getOwner().getLogger().info(
                        "application " + agent.getName() + " will receive it after " + delay_sum);
                final var nextTime = getOwner().getSimulationTime()
                        .getCurrentTick().add(BigDecimal.valueOf(delay_sum));
                getOwner().schedule(
                        new ActionActivator<>(
                                ActivationScheduleFactory.createOneTime(nextTime),
                                action
                        ));
                foundApp = true;
                break;
            }
            if (!foundApp) {
                getOwner().getLogger().warning("could not find a Fabric application to send client puzzle solutions");
            }

        }
    }

    @Override
    public <T extends Action<FabricEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>>> T copy() {
        return (T) new AcNotifyClientsPuzzleSolutionToFabricApp(
                getEnvironment(),
                getOwner(),
                this.clientsNames,
                this.commonDelaySpecification,
                this.addedDelaysPerClient);
    }

}
