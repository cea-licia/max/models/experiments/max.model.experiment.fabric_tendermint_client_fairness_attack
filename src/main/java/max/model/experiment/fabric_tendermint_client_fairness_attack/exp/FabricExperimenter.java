/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.experiment.fabric_tendermint_client_fairness_attack.exp;

import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.model.experiment.fabric_tendermint_client_fairness_attack.mockup.MockupTransactionHasher;
import max.model.ledger.abstract_ledger.approval.AbstractTransactionApprovalPolicy;
import max.model.ledger.simplefabric.env.FabricEnvironment;
import max.model.ledger.simplefabric.role.RFabricApplication;
import max.model.ledger.simplefabric.role.RFabricOrderer;
import max.model.ledger.simplefabric.role.RFabricPeer;
import max.model.ledger.abstract_ledger.approval.AcceptingTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.action.config.AcSetLocalApprovalPolicy;
import max.model.ledger.abstract_ledger.action.config.AcSetLocalLedgerState;
import max.model.ledger.simplefabric.action.AcSetFabricPeerTxApprovalPolicy;
import max.model.ledger.simplefabric.agent.FabricApplication;
import max.model.ledger.simplefabric.agent.FabricOrderer;
import max.model.ledger.simplefabric.agent.FabricPeer;
import max.model.ledger.simplefabric.message.FabricMessageType;
import max.model.ledger.simplefabric.message.handler.FabEndReplyHandler;
import max.model.ledger.simplefabric.message.handler.FabEndRequestHandler;
import max.model.ledger.simplefabric.message.handler.FabSubmitToOrderingHandler;
import max.model.ledger.simplemint.action.AcInitializeTendermintContext;
import max.model.ledger.simplemint.action.AcStartNewTendermintHeight;
import max.model.ledger.simplemint.behavior.TendermintPhase;
import max.model.ledger.simplemint.data_types.hasher.TmHasherSingleton;
import max.model.ledger.simplemint.env.TendermintEnvironment;
import max.model.ledger.simplemint.handler.TmPrecommitMessageHandler;
import max.model.ledger.simplemint.handler.TmPrevoteMessageHandler;
import max.model.ledger.simplemint.handler.TmProposeMessageHandler;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.action.config.AcAddCommunicationDelay;
import max.model.network.stochastic_adversarial_p2p.action.config.AcConfigMessageHandler;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.context.filter.UniformLevelFilter;
import one.util.streamex.StreamEx;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Experimenter for our tests.
 *
 * @author Erwan Mahe
 */
public abstract class FabricExperimenter extends ExperimenterAgent {


    /** Fabric environment. */
    protected FabricEnvironment<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState> fabEnvironment;


    /** Fabric applications. */
    private final Map<String, FabricApplication<PuzzleMockupTransaction>> fabApps;


    /** Fabric peers. */
    private final Map<String, FabricPeer<PuzzleMockupTransaction>> fabPeers;


    /** Fabric peers. */
    private final Map<String, FabricOrderer> fabOrderers;

    /**
     * Build a new experimenter instance.
     */
    public FabricExperimenter() {
        this.fabApps = new HashMap<>();
        this.fabPeers = new HashMap<>();
        this.fabOrderers = new HashMap<>();
    }

    /**
     * Sets up the environment.
     */
    protected void setupEnvironment() {
        // sets up a Tendermint environment for the ordering service of HF
        final var tmEnvironment = new TendermintEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,FabricOrderer>();
        // sets up the hasher for the transactions and vertices
        TmHasherSingleton.getInstance().initialize_concrete_hasher_for_TM_environment(tmEnvironment.getName(),new MockupTransactionHasher());
        // sets up the fabric environment
        this.fabEnvironment = new FabricEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>(tmEnvironment);
    }

    protected void addApplication(FabricApplication app) {
        this.fabApps.put(app.getName(), app);
    }

    public List<String> getApplicationsNames() {
        return this.fabApps.keySet().stream().sorted().collect(Collectors.toList());
    }

    /**
     * Sets up the applications in the simulation.
     * Implementation details are left to the concrete simulation testcase.
     */
    protected abstract void setupApplications();


    protected void addPeer(FabricPeer peer) {
        this.fabPeers.put(peer.getName(), peer);
    }

    public List<String> getPeersNames() {
        return this.fabPeers.keySet().stream().sorted().collect(Collectors.toList());
    }

    /**
     * Sets up the peers in the simulation.
     * Implementation details are left to the concrete simulation testcase.
     */
    protected abstract void setupPeers();


    protected void addOrderer(FabricOrderer orderer) {
        this.fabOrderers.put(orderer.getName(), orderer);
    }

    public List<String> getOrderersNames() {
        return this.fabOrderers.keySet().stream().sorted().collect(Collectors.toList());
    }

    /**
     * Sets up the orderers in the simulation.
     * Implementation details are left to the concrete simulation testcase.
     */
    protected abstract void setupOrderers();


    @Override
    protected List<MAXAgent> setupScenario() {
        this.setupEnvironment();
        this.setupApplications();
        this.setupPeers();
        this.setupOrderers();

        Stream<MAXAgent> apps = this.fabApps.values().stream().map(x -> (MAXAgent) x);
        Stream<MAXAgent> peers = this.fabPeers.values().stream().map(x -> (MAXAgent) x);
        Stream<MAXAgent> ords = this.fabOrderers.values().stream().map(x -> (MAXAgent) x);
        Stream<MAXAgent> resultingStream = StreamEx.of(Stream.of( (MAXAgent) this.fabEnvironment))
                .append(Stream.of( (MAXAgent) this.fabEnvironment.orderingServiceEnvironment))
                .append(apps)
                .append(peers)
                .append(ords);

        return resultingStream.filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Build a plan for Fabric Peers
     */
    protected Plan<FabricApplication<PuzzleMockupTransaction>> buildApplicationPlan(
            Optional<DelaySpecification> baseline_output_delay,
            Optional<DelaySpecification> baseline_input_delay
    ) {
        // Static part
        final List<ActionActivator<FabricApplication<PuzzleMockupTransaction>>> static_plan = new ArrayList<>();
        final var join_commitee_time = getCurrentTick();
        final var setup_time = join_commitee_time.add(BigDecimal.valueOf(1));

        // taking roles
        static_plan.add(new ACTakeRole<FabricApplication<PuzzleMockupTransaction>>(this.fabEnvironment.getName(), RFabricApplication.class,null).oneTime(join_commitee_time));

        // Node setup configuration
        // Setting up the message handlers
        static_plan.add(
                new AcConfigMessageHandler<FabricApplication<PuzzleMockupTransaction>>(
                        this.fabEnvironment.getName(),
                        null,
                        FabricMessageType.ReplyWithEndorsement.name(),
                        new FabEndReplyHandler<>()
                ).oneTime(setup_time)
        );
        // Setting up the delays
        baseline_output_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<FabricApplication<PuzzleMockupTransaction>> add_delay_act = new AcAddCommunicationDelay<FabricApplication<PuzzleMockupTransaction>>(this.fabEnvironment.getName(),
                    null,
                    "OUTPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });
        baseline_input_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<FabricApplication<PuzzleMockupTransaction>> add_delay_act = new AcAddCommunicationDelay<FabricApplication<PuzzleMockupTransaction>>(this.fabEnvironment.getName(),
                    null,
                    "INPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });

        return new Plan<FabricApplication<PuzzleMockupTransaction>>() {
            @Override
            public List<ActionActivator<FabricApplication<PuzzleMockupTransaction>>> getInitialPlan() {
                return static_plan;
            }
        };

    }



    /**
     * Build a plan for Fabric Peers
     */
    protected Plan<FabricPeer<PuzzleMockupTransaction>> buildPeerPlan(
            Optional<DelaySpecification> baseline_output_delay,
            Optional<DelaySpecification> baseline_input_delay
    ) {
        // Static part
        final List<ActionActivator<FabricPeer<PuzzleMockupTransaction>>> static_plan = new ArrayList<>();
        final var join_commitee_time = getCurrentTick();
        final var setup_time = join_commitee_time.add(BigDecimal.valueOf(1));

        // taking roles
        static_plan.add(new ACTakeRole<FabricPeer<PuzzleMockupTransaction>>(this.fabEnvironment.getName(), RFabricPeer.class,null).oneTime(join_commitee_time));

        // Node setup configuration
        // Setting up the message handlers
        static_plan.add(
                new AcConfigMessageHandler<FabricPeer<PuzzleMockupTransaction>>(
                        this.fabEnvironment.getName(),
                        null,
                        FabricMessageType.RequestEndorsement.name(),
                        new FabEndRequestHandler<>()
                ).oneTime(setup_time)
        );
        // Setting up the delays
        baseline_output_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<FabricPeer<PuzzleMockupTransaction>> add_delay_act = new AcAddCommunicationDelay<FabricPeer<PuzzleMockupTransaction>>(this.fabEnvironment.getName(),
                    null,
                    "OUTPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });
        baseline_input_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<FabricPeer<PuzzleMockupTransaction>> add_delay_act = new AcAddCommunicationDelay<FabricPeer<PuzzleMockupTransaction>>(this.fabEnvironment.getName(),
                    null,
                    "INPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });

        return new Plan<FabricPeer<PuzzleMockupTransaction>>() {
            @Override
            public List<ActionActivator<FabricPeer<PuzzleMockupTransaction>>> getInitialPlan() {
                return static_plan;
            }
        };

    }


    /**
     * Build a plan for Fabric Orderers
     */
    protected Plan<FabricOrderer> buildOrdererPlan(
            Optional<DelaySpecification> baseline_output_delay,
            Optional<DelaySpecification> baseline_input_delay,
            int byzantineThresholdF,
            BigDecimal timeoutDuration,
            AbstractTransactionApprovalPolicy<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> ordererApprovalPolicy
    ) {
        // Static part
        final List<ActionActivator<FabricOrderer>> static_plan = new ArrayList<>();
        final var join_commitee_time = getCurrentTick();
        final var setup_time = join_commitee_time.add(BigDecimal.valueOf(1));
        final var start_time = setup_time.add(BigDecimal.valueOf(1));

        // taking roles
        static_plan.add(new ACTakeRole<FabricOrderer>(this.fabEnvironment.getName(), RFabricOrderer.class,null).oneTime(join_commitee_time));
        static_plan.add(new ACTakeRole<FabricOrderer>(this.fabEnvironment.orderingServiceEnvironment.getName(), RTendermintValidator.class,null).oneTime(join_commitee_time));

        // Node setup configuration
        // Setting up the message handlers
        static_plan.add(
                new AcConfigMessageHandler<FabricOrderer>(
                        this.fabEnvironment.getName(),
                        null,
                        FabricMessageType.SubmitEndorsedTransaction.name(),
                        new FabSubmitToOrderingHandler<>()
                ).oneTime(setup_time)
        );
        static_plan.add(
                new AcConfigMessageHandler<FabricOrderer>(
                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                        null,
                        TendermintPhase.PROPOSE.name(),
                        new TmProposeMessageHandler<>()
                ).oneTime(setup_time)
        );
        static_plan.add(
                new AcConfigMessageHandler<FabricOrderer>(
                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                        null,
                        TendermintPhase.PREVOTE.name(),
                        new TmPrevoteMessageHandler<>()
                ).oneTime(setup_time)
        );
        static_plan.add(
                new AcConfigMessageHandler<FabricOrderer>(
                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                        null,
                        TendermintPhase.PRECOMMIT.name(),
                        new TmPrecommitMessageHandler<>()
                ).oneTime(setup_time)
        );

        // Setting up the delays
        baseline_output_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<FabricOrderer> add_delay_act = new AcAddCommunicationDelay<FabricOrderer>(this.fabEnvironment.getName(),
                    null,
                    "OUTPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });
        baseline_input_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<FabricOrderer> add_delay_act = new AcAddCommunicationDelay<FabricOrderer>(this.fabEnvironment.getName(),
                    null,
                    "INPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });
        baseline_output_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<FabricOrderer> add_delay_act = new AcAddCommunicationDelay<FabricOrderer>(this.fabEnvironment.orderingServiceEnvironment.getName(),
                    null,
                    "OUTPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });
        baseline_input_delay.ifPresent(delay -> {
            AcAddCommunicationDelay<FabricOrderer> add_delay_act = new AcAddCommunicationDelay<FabricOrderer>(this.fabEnvironment.orderingServiceEnvironment.getName(),
                    null,
                    "INPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay);
            static_plan.add(add_delay_act.oneTime(setup_time));
        });

        // Setting the local ledger state
        static_plan.add(
                new AcSetLocalLedgerState<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,FabricOrderer>(
                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                        null,
                        new PuzzleMockupLocalLedgerState()
                ).oneTime(setup_time)
        );
        // Setting the transactions approval policy
        static_plan.add(
                new AcSetLocalApprovalPolicy<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,FabricOrderer>(
                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                        null,
                        ordererApprovalPolicy
                ).oneTime(setup_time)
        );
        // Initializes the other context attributed
        static_plan.add(
                new AcInitializeTendermintContext<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,FabricOrderer>(
                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                        null,
                        byzantineThresholdF,
                        timeoutDuration
                ).oneTime(setup_time)
        );

        // To start the first height
        static_plan.add(
                new AcStartNewTendermintHeight<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState,FabricOrderer>(
                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                        null).oneTime(start_time)
        );


        return new Plan<FabricOrderer>() {
            @Override
            public List<ActionActivator<FabricOrderer>> getInitialPlan() {
                return static_plan;
            }
        };

    }

}