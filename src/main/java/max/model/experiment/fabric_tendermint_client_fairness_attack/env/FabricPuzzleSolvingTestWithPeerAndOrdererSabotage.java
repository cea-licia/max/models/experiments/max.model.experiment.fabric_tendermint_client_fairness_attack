/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.experiment.fabric_tendermint_client_fairness_attack.env;

import max.core.action.ACTakeRole;
import max.core.role.RExperimenter;
import max.model.experiment.fabric_tendermint_client_fairness_attack.action.AcNotifyClientsPuzzleSolutionToFabricApp;
import max.model.experiment.fabric_tendermint_client_fairness_attack.exp.FabricExperimenter;
import max.model.ledger.abstract_ledger.action.check.AcCheckCoherence;
import max.model.ledger.abstract_ledger.approval.AbstractTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.approval.AcceptingTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupApprovalPolicy;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransactionGenerator;
import max.model.ledger.abstract_ledger.usecase.puzzle.action.AcCheckOrderFairnessForPuzzles;
import max.model.ledger.simplefabric.agent.FabricApplication;
import max.model.ledger.simplefabric.agent.FabricOrderer;
import max.model.ledger.simplefabric.agent.FabricPeer;
import max.model.ledger.simplefabric.approval.AbstractFabricPeerApprovalPolicy;
import max.model.ledger.simplefabric.approval.AcceptingFabricPeerApprovalPolicy;
import max.model.ledger.simplefabric.role.RFabricChannel;
import max.model.ledger.simplefabric.usecase.puzzle.AcCheckOrderFairnessForPuzzlesInHyperledgerFabric;
import max.model.ledger.simplefabric.usecase.puzzle.PuzzleMockupApplicationSpecificPeerState;
import max.model.ledger.simplefabric.usecase.puzzle.PuzzleMockupFabricPeerApprovalPolicy;
import max.model.ledger.simplemint.behavior.TmProposerSelector;
import max.model.ledger.simplemint.data_types.hasher.TmHasherSingleton;
import max.model.ledger.simplemint.role.RTendermintChannel;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.context.delay.ContinuousUniformDelay;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.msgcount.MessageCounterSingleton;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Level;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;

/**
 * The adversary infects both peers and orderers.
 *
 * @author Erwan Mahe
 */
public class FabricPuzzleSolvingTestWithPeerAndOrdererSabotage {

    @BeforeEach
    public void before(@TempDir Path tempDir) {
        clearParameters();

        PuzzleMockupTransactionGenerator.resetGenerator();
        TmHasherSingleton.resetHasher();
        MessageCounterSingleton.resetCounter();

        setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
        setParameter("MAX_CORE_UI_MODE", "Silent");
        setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
    }


    /**
     * maxNumOfByzOrds = 18 => 18*3 + 1 = 55 orderers
     * **/
    @ParameterizedTest
    @CsvSource({
            /*"18,50,25,3,0,0",
            "18,50,25,3,0,4",
            "18,50,25,3,0,8",
            "18,50,25,3,0,12",
            "18,50,25,3,0,16",
            // ***
            "18,50,25,3,1,0",
            "18,50,25,3,1,4",
            "18,50,25,3,1,8",
            "18,50,25,3,1,12",
            "18,50,25,3,1,16",
            // ***
            "18,50,25,3,2,0",
            "18,50,25,3,2,4",
            "18,50,25,3,2,8",
            "18,50,25,3,2,12",
            "18,50,25,3,2,16",
            // ***
            "18,50,25,3,3,0",
            "18,50,25,3,3,4",
            "18,50,25,3,3,8",
            "18,50,25,3,3,12",
            "18,50,25,3,3,16",
            // ***
            "18,50,25,3,4,0",
            "18,50,25,3,4,4",
            "18,50,25,3,4,8",
            "18,50,25,3,4,12",
            "18,50,25,3,4,16",
            // ***
            "18,50,25,3,5,0",
            "18,50,25,3,5,4",
            "18,50,25,3,5,8",
            "18,50,25,3,5,12",
            "18,50,25,3,5,16",*/
            // ***
            /*"18,50,25,3,6,0",
            "18,50,25,3,6,4",
            "18,50,25,3,6,8",
            "18,50,25,3,6,12",
            "18,50,25,3,6,16",
            // ***
            "18,50,25,3,7,0",
            "18,50,25,3,7,4",
            "18,50,25,3,7,8",
            "18,50,25,3,7,12",
            "18,50,25,3,7,16",
            // ***
            "18,50,25,3,8,0",
            "18,50,25,3,8,4",
            "18,50,25,3,8,8",
            "18,50,25,3,8,12",
            "18,50,25,3,8,16",
            // ***
            "18,50,25,3,9,0",
            "18,50,25,3,9,4",
            "18,50,25,3,9,8",
            "18,50,25,3,9,12",
            "18,50,25,3,9,16",
            // ***
            "18,50,25,3,10,0",
            "18,50,25,3,10,4",
            "18,50,25,3,10,8",
            "18,50,25,3,10,12",
            "18,50,25,3,10,16",*/
            // ***
            /*"18,50,25,3,11,0",
            "18,50,25,3,11,4",
            "18,50,25,3,11,8",
            "18,50,25,3,11,12",
            "18,50,25,3,11,16",
            // ***
            "18,50,25,3,12,0",
            "18,50,25,3,12,4",
            "18,50,25,3,12,8",
            "18,50,25,3,12,12",
            "18,50,25,3,12,16",
            // ***
            "18,50,25,3,13,0",
            "18,50,25,3,13,4",
            "18,50,25,3,13,8",
            "18,50,25,3,13,12",
            "18,50,25,3,13,16",
            // ***
            "18,50,25,3,14,0",
            "18,50,25,3,14,4",
            "18,50,25,3,14,8",
            "18,50,25,3,14,12",
            "18,50,25,3,14,16",
            // ***
            "18,50,25,3,15,0",
            "18,50,25,3,15,4",
            "18,50,25,3,15,8",
            "18,50,25,3,15,12",
            "18,50,25,3,15,16",*/
            // ***
            /*"18,50,25,3,16,0",
            "18,50,25,3,16,4",
            "18,50,25,3,16,8",
            "18,50,25,3,16,12",
            "18,50,25,3,16,16",
            // ***
            "18,50,25,3,17,0",
            "18,50,25,3,17,4",
            "18,50,25,3,17,8",
            "18,50,25,3,17,12",
            "18,50,25,3,17,16",
            // ***
            "18,50,25,3,18,0",
            "18,50,25,3,18,4",
            "18,50,25,3,18,8",
            "18,50,25,3,18,12",
            "18,50,25,3,18,16",
            // ***
            "18,50,25,3,19,0",
            "18,50,25,3,19,4",
            "18,50,25,3,19,8",
            "18,50,25,3,19,12",
            "18,50,25,3,19,16",
            // ***
            "18,50,25,3,20,0",
            "18,50,25,3,20,4",
            "18,50,25,3,20,8",
            "18,50,25,3,20,12",
            "18,50,25,3,20,16",*/
            // ***
            /*"18,50,25,3,21,0",
            "18,50,25,3,21,4",
            "18,50,25,3,21,8",
            "18,50,25,3,21,12",
            "18,50,25,3,21,16",
            // ***
            "18,50,25,3,22,0",
            "18,50,25,3,22,4",
            "18,50,25,3,22,8",
            "18,50,25,3,22,12",
            "18,50,25,3,22,16",
            // ***
            "18,50,25,3,23,0",
            "18,50,25,3,23,4",
            "18,50,25,3,23,8",
            "18,50,25,3,23,12",
            "18,50,25,3,23,16",
            // ***
            "18,50,25,3,24,0",
            "18,50,25,3,24,4",
            "18,50,25,3,24,8",
            "18,50,25,3,24,12",
            "18,50,25,3,24,16",
            // ***
            "18,50,25,3,25,0",
            "18,50,25,3,25,4",
            "18,50,25,3,25,8",
            "18,50,25,3,25,12",
            "18,50,25,3,25,16",*/
            // ***
            // BELOW we exceed the threshold of infected peers
            /*"18,50,25,3,26,0",
            "18,50,25,3,26,4",
            "18,50,25,3,26,8",
            "18,50,25,3,26,12",
            "18,50,25,3,26,16",*/
    })
    public void test(
            int maxNumOfByzOrds,
            int peerCount,
            int numberOfRequiredEndorsements,
            int clientCount,
            int numberOfInfectedPeers,
            int numberOfInfectedOrderers,
            TestInfo testInfo)
            throws Throwable {

        int ordCount = 3 * maxNumOfByzOrds + 1;

        boolean silent = true;

        List<String> clientsList = new ArrayList<>();
        for (int i=0;i< clientCount;i++) {
            clientsList.add("client" + String.valueOf(i));
        }

        String targetClient = clientsList.get(0);


        // Tendermint phase timeout
        BigDecimal tendermintPhaseTimeoutDuration = BigDecimal.valueOf(25);

        // rate of new puzzles
        BigDecimal new_puzzles_rate = BigDecimal.valueOf(10);

        // duration for solving a puzzle
        Optional<DelaySpecification> delayBetweenClientsAndApp = Optional.of(
                new ContinuousUniformDelay(1, 5)
        );

        // delays in communications
        Optional<DelaySpecification> appAndOrderersBaselineDelay = Optional.of(
                new ContinuousUniformDelay(1, 10)
        );
        Optional<DelaySpecification> peerSpecificBaselineDelay = Optional.of(
                new ContinuousUniformDelay(1, 10)
        );

        // Test duration in ticks
        final var testDuration = 20000;

        // Setup tester
        final var tester =
                new FabricExperimenter() {

                    @Override
                    protected void setupApplications() {
                        final var appPlan = buildApplicationPlan(appAndOrderersBaselineDelay,appAndOrderersBaselineDelay);
                        FabricApplication<PuzzleMockupTransaction> app = new FabricApplication<PuzzleMockupTransaction>(appPlan, numberOfRequiredEndorsements);
                        if (silent) {
                            app.getLogger().setLevel(Level.WARNING);
                        }
                        this.addApplication(app);
                    }

                    @Override
                    protected void setupPeers() {
                        int remPeersToInfect = numberOfInfectedPeers;
                        for (var i = 0;
                             i < peerCount;
                             ++i) {
                            AbstractFabricPeerApprovalPolicy<PuzzleMockupTransaction> peerApprovalPolicy;
                            if (remPeersToInfect > 0) {
                                HashSet<String> targetClientInAHashet = new HashSet<>();
                                targetClientInAHashet.add(targetClient);
                                peerApprovalPolicy = new PuzzleMockupFabricPeerApprovalPolicy(
                                        targetClientInAHashet
                                );
                                remPeersToInfect = remPeersToInfect - 1;
                            } else {
                                peerApprovalPolicy = new AcceptingFabricPeerApprovalPolicy<>();
                            }
                            FabricPeer<PuzzleMockupTransaction> peer = new FabricPeer<PuzzleMockupTransaction>(
                                    buildPeerPlan(peerSpecificBaselineDelay, peerSpecificBaselineDelay),
                                    peerApprovalPolicy,
                                    new PuzzleMockupApplicationSpecificPeerState()
                            );
                            if (silent) {
                                peer.getLogger().setLevel(Level.WARNING);
                            }
                            this.addPeer(peer);
                        }
                    }

                    @Override
                    protected void setupOrderers() {
                        int remOrderersToInfect = numberOfInfectedOrderers;
                        for (var i = 0;
                             i < ordCount;
                             ++i) {
                            AbstractTransactionApprovalPolicy<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState> ordererApprovalPolicy;
                            if (remOrderersToInfect > 0) {
                                HashSet<String> targetClientInAHashet = new HashSet<>();
                                targetClientInAHashet.add(targetClient);
                                ordererApprovalPolicy = new PuzzleMockupApprovalPolicy(targetClientInAHashet);
                                remOrderersToInfect = remOrderersToInfect - 1;
                            } else {
                                ordererApprovalPolicy = new AcceptingTransactionApprovalPolicy<>();
                            }
                            FabricOrderer orderer = new FabricOrderer(
                                    buildOrdererPlan(
                                            appAndOrderersBaselineDelay,
                                            appAndOrderersBaselineDelay,
                                            maxNumOfByzOrds,
                                            tendermintPhaseTimeoutDuration,
                                            ordererApprovalPolicy
                                    ),
                                    numberOfRequiredEndorsements
                            );
                            orderer.addLedgerValidatorRole(RTendermintValidator.class);
                            if (silent) {
                                orderer.getLogger().setLevel(Level.WARNING);
                            }
                            this.addOrderer(orderer);
                        }
                        TmProposerSelector.setInstance(this.getOrderersNames());
                    }


                    @Override
                    protected void setupExperimenter() {
                        schedule(
                                new ACTakeRole<>(this.fabEnvironment.getName(),
                                        RExperimenter.class,
                                        this.fabEnvironment)
                                        .oneTime(BigDecimal.ZERO)
                        );
                        schedule(
                                new ACTakeRole<>(this.fabEnvironment.getName(),
                                        RFabricChannel.class,
                                        this.fabEnvironment)
                                        .oneTime(BigDecimal.ZERO)
                        );
                        schedule(
                                new ACTakeRole<>(this.fabEnvironment.orderingServiceEnvironment.getName(),
                                        RTendermintChannel.class,
                                        this.fabEnvironment.orderingServiceEnvironment)
                                        .oneTime(BigDecimal.ZERO)
                        );

                        schedule(
                                new AcNotifyClientsPuzzleSolutionToFabricApp(
                                        this.fabEnvironment.getName(),
                                        this.fabEnvironment,
                                        clientsList,
                                        delayBetweenClientsAndApp,
                                        new HashMap<>())
                                        .repeatFinitely(BigDecimal.valueOf(3),BigDecimal.valueOf(testDuration - 10),new_puzzles_rate)
                        );

                        schedule(
                                new AcCheckCoherence<>(
                                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                                        this.fabEnvironment.orderingServiceEnvironment,
                                        RTendermintValidator.class)
                                        .oneTime(testDuration)
                        );

                        String csfFileDirectory = "C:\\Users\\em244186\\idea_projects\\fabric_tendermint_client_fairness_attack\\";
                        String baseCsvFileName = "puzzle_" +
                                String.valueOf(clientCount) + "clients_" +
                                String.valueOf(peerCount) + "peers_" +
                                String.valueOf(ordCount) + "ords" +
                                "_infP" + numberOfInfectedPeers +
                                "_infO" + numberOfInfectedOrderers;

                        /*String duplicatedCsvFilePath = csfFileDirectory + baseCsvFileName + "_duplicated.csv";
                        schedule(
                                new AcCountDuplicatedDeliveredTransactions<>(
                                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                                        this.fabEnvironment.orderingServiceEnvironment,
                                        RTendermintValidator.class,
                                        Optional.of(Pair.of(duplicatedCsvFilePath,"")))
                                        .oneTime(testDuration)
                        );

                        String msgCountCsvFilePath = csfFileDirectory + baseCsvFileName + "_msg_count.csv";
                        schedule(
                                new AcCollectMessageCount(
                                        this.fabEnvironment.getName(),
                                        this.fabEnvironment,
                                        Optional.of(msgCountCsvFilePath))
                                        .oneTime(testDuration)
                        );*/

                        List<String> targetClientInAList = new ArrayList<>();
                        targetClientInAList.add(targetClient);

                        String prefixForOrderingFairnessCsv = "infPeers;infOrds;targetClientFairness;ordering_rec_ord_fair;ordering_block_ord_fair;ordering_diff_ord_fair\n"
                                + numberOfInfectedPeers + ";" + numberOfInfectedOrderers + ";";
                        String orderingServiceFairnessCsvFilePath = csfFileDirectory + baseCsvFileName + "_fairness_ordering.csv";
                        schedule(
                                new AcCheckOrderFairnessForPuzzles(
                                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                                        this.fabEnvironment.orderingServiceEnvironment,
                                        RTendermintValidator.class,
                                        ordCount / 2,
                                        2 * maxNumOfByzOrds,
                                        targetClientInAList,
                                        clientCount,
                                        Optional.of(Pair.of(orderingServiceFairnessCsvFilePath,prefixForOrderingFairnessCsv)))
                                        .oneTime(testDuration)
                        );
                        String prefixForEndorsingFairnessCsv = "endorsing_rec_ord_fair;endorsing_block_ord_fair;endorsing_diff_ord_fair\n";
                        String endorsingServiceFairnessCsvFilePath = csfFileDirectory + baseCsvFileName + "_fairness_endorsing.csv";
                        schedule(
                                new AcCheckOrderFairnessForPuzzlesInHyperledgerFabric(
                                        this.fabEnvironment.getName(),
                                        this.fabEnvironment,
                                        RTendermintValidator.class,
                                        peerCount / 2,
                                        2 * peerCount / 3,
                                        Optional.of(Pair.of(endorsingServiceFairnessCsvFilePath,prefixForEndorsingFairnessCsv)))
                                        .oneTime(testDuration)
                        );
                    }
                };
        launchTester(tester, testDuration, testInfo);
    }
}
