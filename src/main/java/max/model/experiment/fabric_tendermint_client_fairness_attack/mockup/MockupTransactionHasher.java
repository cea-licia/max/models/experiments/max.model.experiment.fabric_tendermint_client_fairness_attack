/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.experiment.fabric_tendermint_client_fairness_attack.mockup;

import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.simplemint.data_types.hasher.TmAbstractTransactionsHasher;
import org.apache.commons.lang3.tuple.Pair;


/**
 * Defines hash values for mockup transactions
 *
 * @author Erwan Mahe
 */
public class MockupTransactionHasher extends TmAbstractTransactionsHasher<PuzzleMockupTransaction> {
    @Override
    public int make_transaction_hash(PuzzleMockupTransaction mockupTransaction) {
        return Pair.of(mockupTransaction.puzzleIdentifier,mockupTransaction.clientName).hashCode();
    }
}
