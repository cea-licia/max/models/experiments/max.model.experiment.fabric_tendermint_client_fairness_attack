open module max.model.experiment.fabric_tendermint_client_fairness_attack {
    // Exported packages
    exports max.model.experiment.fabric_tendermint_client_fairness_attack.action;
    exports max.model.experiment.fabric_tendermint_client_fairness_attack.env;
    exports max.model.experiment.fabric_tendermint_client_fairness_attack.exp;
    exports max.model.experiment.fabric_tendermint_client_fairness_attack.mockup;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;
    requires max.model.network.stochastic_adversarial_p2p;
    requires max.model.ledger.abstract_ledger;
    requires max.model.ledger.simplemint;
    requires max.model.ledger.simplefabric;

    requires one.util.streamex;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.params;

}