# Some experiments on client fairness attack on a usecase based on HyperLedger Fabric with Tendermint as an ordering service

Let us consider a game in which several clients compete to solve puzzles.
A new puzzle is revealed regularly.
Once a puzzle is revealed, all clients try to solve it.
Once a client solves a puzzle, it sends a transaction with the solution to a distributed ledger.

The ledger totally and irrevocably orders transactions coming from the clients.
A transaction is "delivered"/"finalized" if it is ordered and stored in the distributed ledger.

For each puzzle, we consider that the sender of the first transaction containing its solution wins.
Puzzles being regularly revealed, the fame is fair if, supposing all clients have the same ability to solve puzzles, all clients have the same
likelihood of winning.


In relation to a concrete execution of a system with N clients over a certain implementation of a distributed ledger,
we formalize this notion of client-fairness using a score, defined for each client as follows:
```
score(client) = %win(client) * N
```
where `%win(client)` is the proportion of puzzles that the given client has won during the span of the concrete execution of the system.


If the game is client-fair, then this score converges towards 1 for all clients as the duration of the execution increases 
because the number of resolved games increases (as illustrated below).

<img src="./README_images/score_vs_number_of_games.png">


Depending on the implementation of the distributed ledger that is used to order the puzzle solutions submitted by the clients,
it may be possible for a malicious adversary to attack a specific client, with the aim of reducing its score (reducing its chances of having its solutions taken into account).

In the following we consider several non-trivial attacks over a ledger implemented using HyperLedger Fabric (and Tendermint for the ordering service of HyperLedger Fabric).


## Simplified description of HyperLedger Fabric and Tendermint

HyperLedger Fabric implements a distributed ledger as a blockchain i.e. it batches sequences of transactions into blocks which are progressively created and which content is delivered.

In Hyperledger Fabric we distinguish between:
- Peers, which endorse transactions submitted by clients and forwarded by applications. The set of peers forms an "endorsing service".
- Applications, which forward transactions submitted by clients to the peers, collect endorsements which the peers emit and, upon reaching a certain threshold of endorsements
  (defined up to a certain endorsement policy), forward the endorsed transaction to the orderers for finalization in the blockchain.
- Orderers, which implement a consensus algorithm which aim is the agreement on the next block to add in the blockchain. The ser of orderers form the "ordering service".

We abstract away "Applications" of HyperLedger Fabric which only serve as an entry point to submit transactions.
In the following we consider that for each client, there is a dedicated application which serves as an intermediary between the client and the ledger.

In the following, we also consider a specific implementation of a consensus algorithm by the orderers in the form of Tendermint.

If Tendermint is used, orderers regularly form into committees to decide on the next block to add to the blockchain.
This decision corresponds to a "consensus instance" or "height" (because it coincides with the height of the blockchain).

A consensus instance is organized into rounds which are repeated until a decision is reached.
In a given round, a single proposer node is chosen.
This proposer submits (via broadcast) a proposal which will be submitted to the vote of the other orderers.
We have a two phase vote (PREVOTE and PRECOMMIT) which is designed to ensure Byzantine Fault Tolerance.
If the proposal goes through the two voting phases then it is finalized into a block in the blockchain.

Hence, the Distributed System can be described as follows:

<img src="./README_images/fabric_with_tendermint.png">


## Adversarial model

We consider the adversary model implemented in [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p),
which provides an adversary with the following capabilities:

<img src="./README_images/adversarial_capabilities.png">

See also [this paper](https://arxiv.org/abs/2403.14342) for details.


## Trivial attacks

We do not consider trivial attacks such as:
- preventing the client to broadcast its solutions 
- infecting enough peers so that it is impossible for a transaction coming for the client to be endorsed (up to the endorsing policy)
- infecting more orderers than the byzantine threshold 
(Tendermint is a Byzantine Fault Tolerant consensus algorithm such that a threshold of one third of Byzantine orderers must not be exceeded for the blockchain to ensure coherence)


## Network parameterization and delay attack

In these experimentations we simulate a network implementing the puzzle use case over HyperLedger Fabric and Tendermint.
We use the MAX Multi-Agent eXperimenter multi-agent based simulation tool.

Via a dedicated P2P model :  [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p),, 
we can parameterize communication delays between nodes of the network.

We use the parameterization described below:

<img src="./README_images/network.png">

MAX performing discrete event simulations, the notion of "tick" corresponds to an atomic time step in the simulation.

We use a Uniform Random Distribution of delays (between 1 and 10 simulation ticks) for most input and output events.
We use a specific parameterization for the peers (as we will see, it will be useful to characterize a specific attack on peers).

Via this, we can also simulate a delay attack on the target client.
The adversary may delay the outputs of the target client (e.g., via Denial Of Service (which would also delay its inputs)).
We model this in our simulator via the red clock on the diagram above which corresponds to a fixed delay that is added to all outputs of the target client by the adversary.


## Minority peer sabotage

The adversary can infect a peer so that it refuses to endorse transactions from the target client.
Beyond a certain number of infected peers, it is impossible for the client to have its transactions delivered because
it cannot collect enough endorsements.
However, even if we stay below this threshold, it is still possible for the adversary to negatively impact the target client via infecting a minority of peers.

Let us consider a certain honest peer `p` to which the target client has send a transaction for approval.
Let us consider a certain timestamp `z` that is after the moment of the emission from the client.
Let us then denote by `t` the time at which the client receives the endorsement from the peer `p`.

Hence, we denote by `P(t < z)` the probability that the target client receives the endorsement from `p` before timestamp `z`.

If we suppose all events to be independent (i.e., we
have i.i.d. variables) and have the same likelihood (i.e., peer
to peer channels of communications have equally probable
delays), for any honest peer `p` we may denote by `X` this
probability `P(t < z)`. On the contrary, the endorsement from
`p` being received after `t` has a probability `P(t ≥ z) = 1 − X`.

Among `np` trials, the probability of having exactly `k ≤ np`
peers endorsing the transaction before timestamp `z` is:

<img src="./README_images/peer_sabotage_formula1.png">

Because sabotaged peers never endorse transactions, for sabotage peers, we always have
`X = 0` for any timestamp `z`.
Hence we ignore these peers in the following.

As a result, the probability that the target client collects at least `mp` distinct endorsements before timestamp `z` is the following,
given `bp ≤ np − mp` the number of sabotaged peers :

<img src="./README_images/peer_sabotage_formula2.png">

When plotting Y against X, we can observe the theoretical effect of minority peer sabotage on endorsement time.
The plot below describes a system in which we have `np = 16 peers` and the required number of endorsements is `mp = 10`:

<img src="./README_images/theoretical_effect_peer_sabotage_plot.png">

We can see that the more peers are sabotaged, the smaller is the probability of collecting
enough endorsements before timestamp `z`. By extrapolation,
we conclude that, even if the threshold of sabotaged peers is
not reached, the attack still statistically delays the endorsement
of transactions from the target client. 

As a result, we may obtain the exact same effect as the previous "delay" attack.

In order to confirm this experimentally, we implement this attack using the "inject" action from the adversarial model from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p).

The sequence diagram below describes an example of such attack and its effect:

<img src="./README_images/peer_sabotage.png">

Here, after the adversary infects peer1, it always refuses transactions from client1.



## Minority orderer sabotage

The adversary can infect an orderer so that, if that orderer were to be chosen as a proposer, it would not include transactions from the target client in its block proposal.
Then, because the other orderers would not be able to tell if these transactions were omitted on purpose (it might simply be that the infected orderer has not yet received them),
they would still vote the block.

This attack is illustrated below:

<img src="./README_images/orderer_sabotage.png">

If there are sabotaged orderers, the likelihood of transactions from the target client to be included in the next block diminishes, 
thus negatively impacting its client-fairness score. However, because
the proposer generally isn’t the same from one round to the
next, infecting less than the byzantine threshold of orderers cannot reduce the score
to 0 (total censorship is not possible).





## Simulation and effects of the attacks on client fairness score

MAX performing discrete event simulations, the notion of "tick" corresponds to an atomic time step in the simulation.

In all simulations, we consider 55 = 3*18 + 1 orderers, 
50 peers with at least 25 endorsements (from any 25 distinct peers) being required.

A new puzzle is created every 10 ticks of simulation and
all clients can find a solution between 1 and 5 ticks after
the emission of the puzzle. The Tendermint consensus is
parameterized so that empty blocks can be emitted and the
timeout for each phase is set to 25 ticks. All simulations have
a length of 20000 ticks so that around 2000 puzzles are solved.

In order to assess the delay attack empirically, we performed 16 simulations, 
each corresponding to a specific value for the added delay between 0 and 15.
Simulating the delay attack results in the plot on the left below 
(in which there is a datapoint for each of the 16 simulations).
Slowing down the outputs of the target client
decreases its chance of delivering its solution as another client
solving the puzzle at the same time has more chance of
having its solution being delivered first. This is confirmed
experimentally, as the score decreases with the
value of the delay. This decrease is sharp between 0 and 5 because a puzzle
takes at most 5 ticks to solve (hence, above 5, it is as if the target
client is always the last to solve the puzzle). However, because of
the non determinism and randomness of communications, the score
is not immediately equal to 0 but converges towards this value as
the delay increases.

Likewise, we experimented on the peer sabotage attack via 135 distinct simulations
corresponding to different proportions of infected peers and different parameterizations of
the distribution of delays for the communications involving peers.
The effect of this attack on the client-fairness score of the target client is confirmed experimentally, as illustrated on
the plot on the middle below.
The score of the target client decreases with the proportion of infected peers.
Moreover, we observe that the attack is more efficient if the
baseline network delays are high and random. For the case
where the green URD corresponds to `[1-1]` the effect is only due to the
blue URD affecting the inputs of the clients (thus delaying
the reception of endorsements). However, as illustrated by the
other four cases, if delays in network communications are
non-negligible, then client fairness can indeed be negatively
impacted by infecting a minority of peers.

For the orderer sabotage attack, we performed 21 distinct simulations,
each correspond to a number of infected orderers (between 0 and 20) among the 55 orderers.
As illustrated in the plot on the right below, staying below the Byzantine threshold (one third), we observe a moderate but still significant impact 
the more orderers are infected (around a 25 percent decrease in the score).



|                                                                      |                                                                            |                                                                               |
|----------------------------------------------------------------------|----------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| <img src="./README_images/plots/delays_score_plot_with_borders.png"> | <img src="./README_images/plots/peersabotage_score_plot_with_borders.png"> | <img src="./README_images/plots/orderersabotage_score_plot_with_borders.png"> |


## Impact on order-fairness properties

The property of "client-fairness" which we have studied so far is application specific.
However, its violation essentially boils down to a problem of the ordering of transactions.

Most consensus protocols and blockchains only focus on properties of safety (mainly related to the coherence of the ledger)
and liveness (related to wait-free properties on individual nodes).
However, they rarely uphold "order-fairness" properties as described in 
"[Order-Fairness for Byzantine Consensus](https://link.springer.com/chapter/10.1007/978-3-030-56877-1_16)"
and "[Quick Order Fairness](https://link.springer.com/chapter/10.1007/978-3-031-18283-9_15)".

These properties relate the order of receptions of specific transactions by the different nodes of the system
to the order of delivery of these transactions in the ledger.
In the following, we consider the three properties below (from the two aforementioned papers):
- "receive-order fairness" states that for any two pairs of transactions "t" and "t'" if a majority of nodes
receive t before t' then t must be delivered before t'
- "block-order fairness" states that for any two pairs of transactions "t" and "t'" if a majority of nodes
  receive t before t' then t must not be delivered in a block that is after the block in which t' is delivered
- "differential-order fairness" states that for any two pairs of transactions "t" and "t'" if, counting the number of times
  (across nodes) that t is received before t' minus the number of times t' is received before t, if this count exceed a
certain threshold, then t must be delivered before t'

"receive-order fairness" is the strongest property but it is impossible for an algorithm to updold this property
(Condorcet paradox of t1 being received before t2 by a majority, t2 before t3 by a majority and t3 before t1 by a majority).

"block-order fairness" has the same premise but a weaker consequence while "differential-order fairness" has a more specific
premise. Both can be upheld by concrete algorithms 
(see Aequitas protocols from "[Order-Fairness for Byzantine Consensus](https://link.springer.com/chapter/10.1007/978-3-030-56877-1_16)",
"quick order-fair atomic broadcast protocols" from "[Quick Order Fairness](https://link.springer.com/chapter/10.1007/978-3-031-18283-9_15)").

Hyperledger Fabric and Tendermint do not provide any guarantees in regards to order-fairness.
This is why we are able to successfully perform our attack and decrease the score of the target client.
In the previous 3 diagrams, we have observed the effect of our attacks on that score, however, it is also
interesting to observe the side effects these attacks have on the (violations of the) order fairness properties.

However, in the case of Hyperledger Fabric, there is no single notion of "a majority of nodes" because
there is a distinction between peers and orderers. Thus, the notion
of "reception" used in order-fairness properties can be interpreted in two manners, which yields
considering 6 order-fairness properties instead of 3. Indeed, we can
either consider the receptions of not-yet endorsed transactions by
the peers, or the receptions of endorsed transactions by the orderers.

In the context of our simulations, it is interesting to observe the number of times each order-fairness property is violated
during the span of a given simulation. If these numbers differ depending on the nature and scale of the attack,
this would allow us to observe the impact of that attack on order-fairness.

However, another issue of order-fairness properties is that we must consider all pairs (t,t') of delivered transactions, 
compounded by the number of nodes (on which we must collect receive orders).
This quickly becomes intractable with the length of the simulation and the number of nodes.
Fortunately, we can leverage our use case to define "application-pertinent order-fairness" properties:
in our case, we only consider pairs (t,t') of transactions that concern the same puzzle (i.e. any two solutions of the same puzzle
send by different clients).

The six diagrams below represent the number of times these "application-pertinent order-fairness" properties have been violated
in our 307 simulations.


|                                                                      |                                                                            |                                                                               |
|----------------------------------------------------------------------|----------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| <img src="./README_images/plots/delays_endorsing_fairness_plot_with_borders.png"> | <img src="./README_images/plots/peersabotage_endorsing_fairness_plot_with_borders.png"> | <img src="./README_images/plots/orderersabotage_endorsing_fairness_plot_with_borders.png"> |
| <img src="./README_images/plots/delays_ordering_fairness_plot_with_borders.png"> | <img src="./README_images/plots/peersabotage_ordering_fairness_plot_with_borders.png"> | <img src="./README_images/plots/orderersabotage_ordering_fairness_plot_with_borders.png"> |

In the case of the delay attack (two left-most diagrams above),
when the delay is 0, we observe that there are
order-fairness violations of all three kinds and w.r.t. both peers and
orderers. This is because both Tendermint and HF do not uphold
order-fairness properties. Because the delay is added before both peers
and orderers receive the affected transactions, this attack has no
direct effect on order-fairness. However, there is an indirect impact
due to the fact that there is less competition between transactions
from the target client and the other clients. Indeed, because these
transactions are then more likely to be both received and ordered
after those of the other clients for the same puzzle, there are less
risks of order fairness violations. Interestingly, in our simulations
we have three clients, and, after putting out of commission one of
these, the number of violations for all six properties roughly decreases by two thirds.

In the case of the peer sabotage attack (two diagrams on the middle column), in order to avoid drawing 15 curves
on each diagram, we only keep the two extreme cases for the delay
parameterization: "between 1 and 1 ticks" and "between 1 and 20 ticks". We distinguish the 6 remaining
curves on each diagram via their colors and both the
line style and the shape of datapoints. The number of violations for
all three order fairness properties defined w.r.t. to the peers (top
diagram) increase with the proportion of infected peers. This is
especially the case for the "1-20 ticks" URD of delays. This is due to the
fact that this attack impacts the delivery order of transactions but
does not impact their reception order by peers, thus increasing the
number of discrepancies. Concerning the three properties defined
w.r.t. the orderers (bottom diagram), the effect of this attack is identical to that of the delay attack because, 
from the perspective of the orderers,
it likewise amounts to adding a delay for the reception of endorsed
transactions from the target client. If more than half of the peers are
infected, no transactions from the target client are endorsed. Hence
its score drops to 0 and the number of violations also decreases
for the same reasons as in the case of the delay attack 
(less competitions and hence less risks of order-fairness violations).

As for the orderer sabotage attack (two diagram on the right column), because this attack has no
effect on the order of reception of transactions, w.r.t. neither the
peers nor the orderers, its only impact on order-fairness properties
lies in the order of delivery. Block-order fairness is particularly
impacted as sabotaged orderers, if chosen as proposers, will often
cause violations of this property. Above 33% of infected orderers,
blocks containing transactions from the target client cannot collect
enough PRECOMMIT messages to be chosen and, as a result, the
score drops to 0 and order fairness violations decrease due to the
lack of competition.

## Combining peer sabotage and orderer sabotage

Finally, we have experimented with combining peer and orderer sabotage.
We have fixed the peer to peer Uniform Random Distribution of delays for peers to be between 1 and 10 ticks.

We performed 135 simulations with the proportion of infected peers varying between 0% and 52% and that
of infected orders between 0% and 29%.

The 5 curves on the plot on the left and below each correspond to different proportions of infected orderers.
We observe that the more there are
infected peers and orderers, the more the score decreases. 
However, there are diminishing returns between the number of infected
peers and orderers. Indeed, when the proportion of infected peers
is low, the infection of orderers has a significant effect. By contrast,
close to 50% of infected peers, we can see that there is few to no
advantage in infecting additional orderers below the BFT threshold.

|                                                                      |                                                                            |                                                                                            |
|----------------------------------------------------------------------|----------------------------------------------------------------------------|--------------------------------------------------------------------------------------------|
| <img src="./README_images/plots/multiplesabotage_score_plot_with_borders.png"> | <img src="./README_images/plots/multiplesabotage_endorsing_fairness_plot_with_borders.png"> | <img src="./README_images/plots/multiplesabotage_ordering_fairness_plot_with_borders.png"> |



## How to reproduce the results

### delay attack

In the "FabricPuzzleSolvingTestWithDelay", change the "csfFileDirectory" variable to set the path towards the directory in which you want test results (in the form of CSV files) to be created.

Run the "FabricPuzzleSolvingTestWithDelay" test suite to run the 16 simulations corresponding to the delay attacks.
This will (re)generate the corresponding `.csv` files that are in the folder `experiment_results`.

### peer sabotage attack

In the "FabricPuzzleSolvingTestWithPeerSabotage", change the "csfFileDirectory" variable to set the path towards the directory in which you want test results (in the form of CSV files) to be created.

Run the "FabricPuzzleSolvingTestWithPeerSabotage" test suite to run the 135 simulations corresponding to the peer sabotage attacks.
This will (re)generate the corresponding `.csv` files that are in the folder `experiment_results`.

### orderer sabotage attack

In the "FabricPuzzleSolvingTestWithOrdererSabotage", change the "csfFileDirectory" variable to set the path towards the directory in which you want test results (in the form of CSV files) to be created.

Run the "FabricPuzzleSolvingTestWithOrdererSabotage" test suite to run the 21 simulations corresponding to the orderer sabotage attacks.
This will (re)generate the corresponding `.csv` files that are in the folder `experiment_results`.

### peer and orderer sabotage attack

In the "FabricPuzzleSolvingTestWithPeerAndOrdererSabotage", change the "csfFileDirectory" variable to set the path towards the directory in which you want test results (in the form of CSV files) to be created.

Run the "FabricPuzzleSolvingTestWithPeerAndOrdererSabotage" test suite to run the 135 simulations corresponding to specific proportions of infected peers and orderers.
This will (re)generate the corresponding `.csv` files that are in the folder `experiment_results`.

### Plot the data

In order to plot the data, you need R installed (see [r-project.org](https://www.r-project.org/)).

Use the following command to plot the data:

| on UNIX | on Windows |
|---------|------------|
|`Rscript plot_script.R`| `Rscript.exe .\plot_script.R` |

You can specify the folder in which to retrieve the 614 `.csv` files
via the "base_file_name" variable in `plot_script.R`.

# Licence

__max.model.experiment.fabric_tendermint_client_fairness_attack__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)













